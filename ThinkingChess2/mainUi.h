#pragma once
#include <QWidget>
#include <ThinkingChess2.h>
#include <QPushButton>
#include <QLabel>

class mainUi : public QWidget
{
	Q_OBJECT

public:
	mainUi(QWidget *parent = nullptr);
//private slots:
//	void normalbg();
private:
	QLabel *bgLabel;
	QPushButton *startButton;
	QLabel *randLabel;
};
