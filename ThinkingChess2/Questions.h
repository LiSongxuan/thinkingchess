#pragma once
#include <QtWidgets/QMainWindow>
#include <qvector.h>
#include <qpushbutton.h>
#include <qstatusbar.h>
#include <qtextcodec.h>
#include <qaction.h>
#include <qtoolbar.h>
#include <qmessagebox.h>
#include <qdialog.h>
#include <qlabel.h>
#include <qmovie.h>

class mybutton :public QPushButton
{
    Q_OBJECT
signals:
    void chosen(mybutton *b);
private:
    bool dirtybit;//用于判定按钮是否已被选择
    bool anwseringbit;//用于判定按钮是否处在答题状态，即在某一阶段点击空或选项是否应做出响应
    int mx, my;//存储坐标用于复位
    bool san;//存储初始答题状态位的值用于复位
public:
    mybutton(int x, int y, QString s, bool bit, QWidget *parent = Q_NULLPTR);
    mybutton(int x, int y, QIcon icon, bool bit, QWidget *parent = Q_NULLPTR);
    int serialn;//按钮的编号，用于填入数组最终判断是否正确
    void broadcast();//符合响应条件的按钮被点击时发出信号并将选择位置为已被选择
    bool check();//返回值为答题状态位
    void save();//用于存储按钮的信息（用于复位）
    void change();//用于更新按钮的答题状态位
    void reset();//用于将按钮复位
};

class Questions;
class mymsg :public QMainWindow
{
    Q_OBJECT
signals:
    void ack();
public:
    mymsg(Questions *parent, int mood);
private:
    QPushButton *know;
    QLabel *bgp;
    QLabel *t;
    QLabel *mov;
    void known();
};

class Questions;
class mymsg :public QMainWindow
{
	Q_OBJECT
signals:
	void ack();
public:
	mymsg(Questions *parent, int mood);
private:
	QPushButton *know;
	QLabel *bgp;
	QLabel *t;
	QLabel *mov;
	void known();
};

class Questions : public QMainWindow
{
    Q_OBJECT
signals:
    void ensurer();
    void ensures();
    void incorrect();
    void correct();
    void success(Questions *p, int failure, int difficult);
public:
    Questions(int n, QMainWindow *parent = Q_NULLPTR);//n是题目的编号，根据编号的不同构造出不同的窗口
private:
<<<<<<< HEAD
    QAction *e1;//重新答题（复位所有按钮）
    QAction *e2;//提交（开始判定答题正误）
    QToolBar *op;//进行上述操作的工具栏

    mymsg * fr;
    mymsg * ff;//用于反馈正误信息的窗口

    QLabel *bgp;//背景图片

    QLabel *t;//题目中用于显示文字信息的标签

    QLabel *la;
    QLabel *lb;
    QLabel *lc;
    QLabel *ld;
    QLabel *le;//在题目中用于显示图片的标签

    mybutton *qa;
    mybutton *qb;
    mybutton *qc;//题目中的空

    mybutton *aa;
    mybutton *ab;
    mybutton *ac;
    mybutton *ad;
    mybutton *ae;//选项

    QVector<mybutton *> answers;//存储空
    QVector<mybutton *> blanks;//存储选项

    int difficult;//难度,分为1~5级
    int failure;//记录失败次数
    int qx, qy;//用于存储按钮的坐标
    int ans[3];//玩家填入的答案
    int solution[3];//标准答案
    int *pta;//用于指定ans的一个位置

    void startanswer();//只有选中空格时才会显示选项
    void fill(mybutton *a);//选项被选中时将其移到已选择的空位
    void save(mybutton *q);//空位被选择时存储其位置用于选项的填入
    void reset();//用于将关卡复位
    void proof();//完成选择后判定是否正确
    void prereset();//复位前创造对话框确认是否复位
    void presubmit();//提交前创造对话框确认是否提交
    void comfort();//答题错误，安慰一下
    void congratulation();//答题正确，表扬一下
    void again();//做错之后的重置
    void forward();//传回
    void back();
};

class mywindow :public QMainWindow
{
    Q_OBJECT
signals:
    void stage(int n);
public:
    mywindow();
private:
    QPushButton *b1;
    Questions* q[25];

    QLabel *l1;

    QVector <Questions *> d1;
    QVector <Questions *> d2;
    QVector <Questions *> d3;
    QVector <Questions *> d4;
    QVector <Questions *> d5;//用于随机出现题目，分别对应五个难度分级

    QVector <int> d1f;
    QVector <int> d2f;
    QVector <int> d3f;
    QVector <int> d4f;
    QVector <int> d5f;//用于存储每一难度分级下遭遇题目的失误次数，用于计算分数

    int f;
    int dif;
    void mf(int n);
    void select();
    void back(Questions *p, int n, int m);
=======
	QAction *e1;//重新答题（复位所有按钮）
	QAction *e2;//提交（开始判定答题正误）
	QToolBar *op;//进行上述操作的工具栏

	mymsg * fr;
	mymsg * ff;//用于反馈正误信息的窗口

	QLabel *bgp;//背景图片
	
	QLabel *t;//题目中用于显示文字信息的标签
	
	QLabel *la;
	QLabel *lb;
	QLabel *lc;
	QLabel *ld;
	QLabel *le;//在题目中用于显示图片的标签
	
	mybutton *qa;
	mybutton *qb;
	mybutton *qc;//题目中的空

	mybutton *aa;
	mybutton *ab;
	mybutton *ac;
	mybutton *ad;
	mybutton *ae;//选项

	QVector<mybutton *> answers;//存储空
	QVector<mybutton *> blanks;//存储选项

	int difficult;//难度,分为1~5级
	int failure;//记录失败次数
	int qx, qy;//用于存储按钮的坐标
	int ans[3];//玩家填入的答案
	int solution[3];//标准答案
	int *pta;//用于指定ans的一个位置

	void startanswer();//只有选中空格时才会显示选项
	void fill(mybutton *a);//选项被选中时将其移到已选择的空位
	void save(mybutton *q);//空位被选择时存储其位置用于选项的填入
	void reset();//用于将关卡复位
	void proof();//完成选择后判定是否正确
	void prereset();//复位前创造对话框确认是否复位
	void presubmit();//提交前创造对话框确认是否提交
	void comfort();//答题错误，安慰一下
	void congratulation();//答题正确，表扬一下
	void again();//做错之后的重置
	void forward();//传回
	void back();
>>>>>>> 80b347b57c5cb6fc32933a57b3cb5c59ecb6ff57
};

