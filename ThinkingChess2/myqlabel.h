#ifndef MYQLABEL_H
#define MYQLABEL_H
#include <QLabel>

class myQLabel:public QLabel
{
    Q_OBJECT
public:
    myQLabel(QWidget * parent=nullptr);
protected:
    void mousePressEvent(QMouseEvent *);

signals:
    void on_click();
public slots:

};

#endif // MYQLABEL_H
