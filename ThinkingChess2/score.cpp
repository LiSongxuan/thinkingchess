#include "score.h"

void score::showscore(QMainWindow * outthis)
{
    QString q1 = QString("你的总分是:%1(满分37）\n").arg(scoreresult);
    QString q2 = QString("其中第一部分得分：%1（总分2分）\n").arg((double)scorelist[0] / (double)times[0]);
    QString q3 = QString("其中第二部分得分：%1（总分4分）\n").arg((double)scorelist[1] / (double)times[1]);
    QString q4 = QString("其中第三部分得分：%1（总分6分）\n").arg((double)scorelist[2] / (double)times[2]);
    QString q5 = QString("其中第四部分得分：%1（总分10分）\n").arg((double)scorelist[3] / (double)times[3]);
    QString q6 = QString("其中第五部分得分：%1（总分15分）\n").arg((double)scorelist[4] / (double)times[4]);
    QMessageBox msg(outthis);
    msg.setWindowTitle("成绩分析");
    msg.setText(q1+q2+q3+q4+q5+q6);
    msg.setStandardButtons(QMessageBox::Ok);
    if (msg.exec() == QMessageBox::Ok)
        return;
}
