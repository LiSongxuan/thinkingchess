#include "ThinkingChess2.h"
#include "mainUi.h"
#include <QMovie>
#include <QDebug>
#include <QTimer>
#include <QFileInfo>
ThinkingChess2::ThinkingChess2(QWidget *parent)
    : QMainWindow(parent)
{
    num = 1;
    ui.setupUi(this);
    QPixmap pix(":/ThinkingChess2/Resources/startBg.png");
    pix = pix.scaled(QSize(1200, 800), Qt::KeepAspectRatio);
    q.setParent(this);
    QPixmap img(":/ThinkingChess2/Resources/stone.png");
    q.setGeometry(65, 660, 90, 90);
    q.setPixmap(img);

    QPixmap sButtonpic(":/Resources/startButton.png");
    sButtonpic = sButtonpic.scaled(QSize(300, 180), Qt::KeepAspectRatio);
    this->ui.startButton->setIconSize(QSize(300, 180));
    this->ui.startButton->setIcon(sButtonpic);
    this->ui.startButton->update();

    QPixmap homeButtonpic(":/Resources/backToMenu.png");
    homeButtonpic = homeButtonpic.scaled(QSize(130, 130), Qt::KeepAspectRatio);
    this->ui.mainMenuButton->setIconSize(QSize(130, 130));
    this->ui.mainMenuButton->setIcon(homeButtonpic);
    this->ui.mainMenuButton->update();

    pPosAnimation1 = new QPropertyAnimation(&q, "pos");
    pPosAnimation1->setDuration(230);
    pPosAnimation1->setEasingCurve(QEasingCurve::InOutQuad);
    begin.setLoopCount(QSoundEffect::Infinite);
    playing.setLoopCount(QSoundEffect::Infinite);
    QFileInfo fi("Resources/newGame.wav");
    QFileInfo fi2("Resources/game.wav");
    begin.setSource(QUrl::fromLocalFile(fi.absoluteFilePath()));
    playing.setSource(QUrl::fromLocalFile(fi2.absoluteFilePath()));
    begin.play();
    begin.setVolume(1.0);
    playing.setVolume(1.0);
    this->update();

    this->ui.bgLabel->setPixmap(pix);
    this->ui.bgLabel->update();

    this->ui.gameStartButton->setEnabled(false);
    this->ui.gameStartButton->hide();
    this->ui.mainMenuButton->hide();
    this->q.hide();
    //q.setDisabled(true);
<<<<<<< HEAD
    setFixedSize(1200, 800);
    setWindowTitle("test");
=======
	setFixedSize(1200, 800);
	setWindowTitle("test");
>>>>>>> 80b347b57c5cb6fc32933a57b3cb5c59ecb6ff57
    int i;
    for(i = 0;i < 25;++i)
    {
        qs[i] = new Questions(i, this);
    }


    d1.push_back(qs[0]);
    d1.push_back(qs[1]);
    d1.push_back(qs[2]);

    d2.push_back(qs[8]);
    d2.push_back(qs[9]);
    d2.push_back(qs[11]);
    d2.push_back(qs[13]);
    d2.push_back(qs[17]);
    d2.push_back(qs[20]);

    d3.push_back(qs[14]);
    d3.push_back(qs[15]);
    d3.push_back(qs[19]);
    d3.push_back(qs[21]);
    d3.push_back(qs[22]);
    d3.push_back(qs[24]);

    d4.push_back(qs[3]);
    d4.push_back(qs[6]);
    d4.push_back(qs[7]);
    d4.push_back(qs[10]);
    d4.push_back(qs[23]);
<<<<<<< HEAD
=======

    d5.push_back(qs[4]);
    d5.push_back(qs[5]);
    d5.push_back(qs[12]);
    d5.push_back(qs[16]);
    d5.push_back(qs[18]);
>>>>>>> 80b347b57c5cb6fc32933a57b3cb5c59ecb6ff57

    d5.push_back(qs[4]);
    d5.push_back(qs[5]);
    d5.push_back(qs[12]);
    d5.push_back(qs[16]);
    d5.push_back(qs[18]);

<<<<<<< HEAD
    connect(this, &ThinkingChess2::stage, this, &ThinkingChess2::mf);
=======
    for(i = 0;i<25;++i)
    {
        connect(qs[i], &Questions::success, this, &ThinkingChess2::back);
    }
>>>>>>> 80b347b57c5cb6fc32933a57b3cb5c59ecb6ff57

    for(i = 0;i<25;++i)
    {
        connect(qs[i], &Questions::success, this, &ThinkingChess2::back);
    }

    connect(this->ui.startButton, SIGNAL(clicked()), this, SLOT(bgChange()));
    connect(this->ui.startButton, SIGNAL(clicked()), this->ui.startButton, SLOT(hide()));
    connect(this->ui.startButton, SIGNAL(clicked()), this->ui.gameStartButton, SLOT(show()));
    connect(this->ui.startButton, SIGNAL(clicked()), this->ui.mainMenuButton, SLOT(show()));
    connect(this->ui.startButton, SIGNAL(clicked()), &(this->q), SLOT(show()));
    connect(this->ui.gameStartButton, SIGNAL(clicked()), this->ui.mainMenuButton, SLOT(hide()));
    connect(this->ui.gameStartButton, SIGNAL(clicked()), this, SLOT(roll()));
    connect(&(this->q), SIGNAL(on_click()), this->ui.mainMenuButton, SLOT(hide()));
    connect(&(this->q), SIGNAL(on_click()), this, SLOT(roll()));
    connect(this->ui.mainMenuButton, SIGNAL(clicked()), this, SLOT(backToMenu()));

    connect(this, SIGNAL(reachedFinal()), this, SLOT(finalChange()));
    //connect(this->)
//	connect(this->ui.restartButton, SIGNAL(clicked()), this, SLOT(finalChange()));
}
void ThinkingChess2::bgChange()
{
    begin.stop();
    playing.play();
    this->ui.rollLabel->show();
    this->ui.gameStartButton->setEnabled(true);
    this->ui.bgLabel->show();
    QPixmap pix2(":/ThinkingChess2/Resources/bg.png");
    pix2 = pix2.scaled(QSize(1200, 800), Qt::KeepAspectRatio);
    this->ui.bgLabel->setPixmap(pix2);
    this->ui.bgLabel->update();
}
void ThinkingChess2::rollStart(int frameNum)
{
    num = frameNum;
    rollMovie = new QMovie(":/ThinkingChess2/Resources/movie.gif");
    connect(rollMovie, SIGNAL(started()), this, SLOT(timer()));
    connect(rollMovie, SIGNAL(updated(const QRect)), this, SLOT(reached()));
    rollMovie->setScaledSize(this->ui.rollLabel->size());
    rollMovie->setSpeed(300);
    rollMovie->setCacheMode(QMovie::CacheAll);
    this->ui.rollLabel->setMovie(rollMovie);
    rollMovie->start();
}

void ThinkingChess2::timer()
{
    this->ui.gameStartButton->setEnabled(false);
    timecount = new QTimer(this);
    connect(timecount, SIGNAL(timeout()), this, SLOT(timeup()));
    timecount->start(1000);
}
void ThinkingChess2::timeup()
{
    if (rollMovie->speed() > 50)
    rollMovie->setSpeed(rollMovie->speed()/2);
}
void ThinkingChess2::reached()
{
    if(rollMovie->currentFrameNumber() == frames[num])
        if (rollMovie->speed() < 50)
        {
            rollMovie->stop();
            animation(num);
            if (piece.rindex()==16)
                emit reachedFinal();
    }

}
void ThinkingChess2::finalChange(int score)
{
    this->ui.gameStartButton->deleteLater();
    QPixmap pix3(":/ThinkingChess2/Resources/finalBg.png");
    pix3 = pix3.scaled(QSize(1200, 800), Qt::KeepAspectRatio);
    this->ui.bgLabel->setPixmap(pix3);
    this->ui.rollLabel->hide();
    this->q.hide();
    this->ui.mainMenuButton->hide();
    this->ui.bgLabel->update();
    sco.calscore();
    sco.showscore(this);
}

void ThinkingChess2::caseClosed()
{
    this->ui.gameStartButton->setEnabled(true);
    ui.mainMenuButton->show();
    QPixmap pix4(":/ThinkingChess2/Resources/goOn.png");
    pix4 = pix4.scaled(QSize(166, 103), Qt::KeepAspectRatio);
    this->ui.gameStartButton->setIconSize(QSize(166, 103));
    this->ui.gameStartButton->setIcon(pix4);
    this->ui.gameStartButton->update();
}
void ThinkingChess2::backToMenu()
{
    playing.stop();
    begin.play();
    QPixmap pix(":/ThinkingChess2/Resources/startBg.png");
    pix = pix.scaled(QSize(1200, 800), Qt::KeepAspectRatio);
    this->ui.bgLabel->setPixmap(pix);
    this->ui.bgLabel->update();
    this->ui.startButton->show();
    this->ui.gameStartButton->hide();
    this->ui.mainMenuButton->hide();
    this->ui.rollLabel->hide();
    this->q.hide();
    this->ui.gameStartButton->setEnabled(false);
}
void ThinkingChess2::roll()
{
    int num = this->piece++;
    this->rollStart(num);
    return;
}

void ThinkingChess2::mf(int n)
{
    int i;
    qsrand(time(0));
    hide();
    switch (n)
    {
    case 1:
    {
        i = qrand() % (d1.size());
        d1[i]->show();
        d1.erase(d1.begin() + i);
        break;
    }
    case 2:
    {
        i = qrand() % (d2.size());
        d2[i]->show();
        d2.erase(d2.begin() + i);
        break;
    }
    case 3:
    {
        i = qrand() % (d3.size());
        d3[i]->show();
        d3.erase(d3.begin() + i);
        break;
    }
    case 4:
    {
        i = qrand() % (d4.size());
        d4[i]->show();
        d4.erase(d4.begin() + i);
        break;
    }
    default:
    {
        i = qrand() % (d5.size());
        d5[i]->show();
        d5.erase(d5.begin() + i);
        break;
    }
    }
}
void ThinkingChess2::back(Questions *p, int n, int m)
{
    caseClosed();
    show();
    p->close();
    sco.plus(n,m);
    switch (m)
    {
    case 1:
    {
        d1f.push_back(n);
        break;
    }
    case 2:
    {
        d2f.push_back(n);
        break;
    }
    case 3:
    {
        d3f.push_back(n);
        break;
    }
    case 4:
    {
        d4f.push_back(n);
        break;
    }
    default:
    {
        d5f.push_back(n);
        break;
    }
    }
}
QLabel* ThinkingChess2::aniLabel()
{
    return &q;
}
//void ThinkingChess2::paintEvent(QPaintEvent *)
//{
  //  pic.load(":/ThinkingChess2/Resources/stone.png");
    //QPainter painter(this);
    //painter.drawPixmap(64,660,90,90,pic);
   //painter.drawLine(QPointF(0, 0), QPointF(100, 100));
//}
