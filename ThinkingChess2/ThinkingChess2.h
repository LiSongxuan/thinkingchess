#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ThinkingChess2.h"
#include "piece.h"
#include <QLabel>
#include "Questions.h"
#include "score.h"
#include <QPainter>
#include <QPixmap>
#include "myqlabel.h"
#include <QSoundEffect>


class ThinkingChess2 : public QMainWindow
{
    Q_OBJECT

public:
    ThinkingChess2(QWidget *parent = Q_NULLPTR);
    QMovie *rollMovie;
    QTimer *timecount;
    QLabel *aniLabel();
    void animation(int num)
    {
        int i = piece.rindex()-num;
        for (int k = 0; k < num; k++)
        {
            pPosAnimation1->setStartValue(piece[i]);
            pPosAnimation1->setEndValue(piece[i+1]);
            pPosAnimation1->start();
            i++;
            Delay_MSec(230);
        }
        Delay_MSec(2000);
        if(piece.rindex() != 16)
        emit stage(((piece.rindex()-1) / 3) + 1);
    }
    void Delay_MSec(unsigned int msec)
    {
        QEventLoop loop;//定义一个新的事件循环
        QTimer::singleShot(msec, &loop, SLOT(quit()));//创建单次定时器，槽函数为事件循环的退出函数
        loop.exec();//事件循环开始执行，程序会卡在这里，直到定时时间到，本循环被退出
    }



private:
    Ui::ThinkingChess2Class ui;
    score sco;
    const int frames[4] = { 0,13,8,3 };
    int num = 0;
    myQLabel q;
    PIECE piece;
    QPropertyAnimation * pPosAnimation1;
    QPixmap pic;
    QSoundEffect begin;
    QSoundEffect playing;


    Questions *qs[25];

    QVector <Questions *> d1;
    QVector <Questions *> d2;
    QVector <Questions *> d3;
    QVector <Questions *> d4;
    QVector <Questions *> d5;//用于随机出现题目，分别对应五个难度分级

    QVector <int> d1f;
    QVector <int> d2f;
    QVector <int> d3f;
    QVector <int> d4f;
    QVector <int> d5f;//用于存储每一难度分级下遭遇题目的失误次数，用于计算分数

    int dif;
    void mf(int n);
    void back(Questions *p, int n, int m);

private slots:
    void bgChange();
    void rollStart(int num = 1);
    void timer();
    void timeup();
    void reached();
    void caseClosed();
    void finalChange(int score = 0);
    void backToMenu();
    void roll();
signals:
    void reachedFinal();
    void stage(int n);
};
