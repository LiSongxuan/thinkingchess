#include "piece.h"
#include <time.h>
#include <cstdlib>
#include <QLabel>
int points[17 * 2] =
{ 109,705,315,745,460,745,605,745,750,746,750,603,605,603,457
,556,457,410,457,268,314,215,315,66,457,66,604,66,750,66,888,
66,1080,88 };
PIECE::PIECE()
{
	for (int i = 0; i < 17 * 2 - 1; i+=2)
		cb.append(QPoint(points[i]-45, points[i + 1]-45));
	index = 0;
	r = 48;
}
int PIECE::operator++(int)
{
	srand(time(0));
	int a = rand() % 3 + 1;
	if (index + a > 16)
		a = 16 - index;
	index += a;
	return a;
}