#pragma once
#include <QMainWindow>
#include <QMessageBox>

class score
{
private:
    int scorelist[5];
    int times[5];
    double scoreresult;
public:
    score(void)
    {
        for (int i = 0; i < 5; i++)
        {
            scorelist[i] = 0;
            times[i] = 0;
        }
        scoreresult = 0;
    }
    void plus(int n,int m)
    {
        n++;
        m--;
        times[m]++;
        int list[] = { 2,4,6,10,15 };
        if (m == 0 || m == 1)
        {
            if (n == 1)
                scorelist[m] += list[m];
        }
        else
        {
            if (n == 1)
                scorelist[m] += list[m];
            else if (n == 2)
                scorelist[m] += (double)list[m] / 2.0;
        }
    }
    double  rsocre(void)
    {
        return scoreresult;
    }
    void calscore(void)
    {
        for (int i = 0; i < 5; i++)
        {
            scoreresult += (double)scorelist[i] / (double)times[i];
        }
    }
    void showscore(QMainWindow * outthis);
};


 // SCORE_H
