#pragma once
#include <QtWidgets/QWidget>
#include <QVector>
#include <QString>
#include <QTimer>
#include <QEventLoop>
#include <QPropertyAnimation>

class PIECE
{
//	Q_OBJECT
private:
	int r;
	int index;
	QVector<QPoint> cb;
	//QString file;
	
public:
	/*void animation(int num)
	{
		int i = index;
		for (int k = 0; k < num; k++)
		{
			pPosAnimation1->setStartValue(QPoint(cb[i]));
			pPosAnimation1->setEndValue(QPoint(cb[i+1]));
			pPosAnimation1->start();
			i++;
			Delay_MSec(230);
		}
	}
	void Delay_MSec(unsigned int msec)
	{
		QEventLoop loop;//定义一个新的事件循环 
		QTimer::singleShot(msec, &loop, SLOT(quit()));//创建单次定时器，槽函数为事件循环的退出函数 
		loop.exec();//事件循环开始执行，程序会卡在这里，直到定时时间到，本循环被退出 
	}
	*/

	void send(void)
	{
		emit steps((*this)++);
	}
signals:
	void steps(int s);
	int rindex(void) { return index; }
	void adjustr(int a) { r = a; }
	int operator++(int);
	QPoint operator[](int index)
	{
		return cb[index];
	}
	
	PIECE(void);
	~PIECE() {}
};
